package xyz.zalya.trivia;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import xyz.zalya.trivia.data.AnswerListAsyncResposne;
import xyz.zalya.trivia.data.QuestionBank;
import xyz.zalya.trivia.model.Question;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private TextView questionTextview;
    private TextView questionCounterTextview;
    private Button trueButton;
    private Button falseButton;
    private ImageButton nextButton;
    private ImageButton prevButton;
    private int currentQuestionIndex = 0;
    private TextView highestScore;
    private TextView currentScore;
    private int currScore = 0;
    private int highScore;
    private List<Question> questionList;
    private final static String MESSAGE_ID = "highest_score";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nextButton = findViewById(R.id.next_button);
        prevButton = findViewById(R.id.prev_button);
        trueButton = findViewById(R.id.true_button);
        falseButton = findViewById(R.id.false_button);
        questionTextview = findViewById(R.id.question_textview);
        questionCounterTextview = findViewById(R.id.counter);
        highestScore = findViewById(R.id.highest_score);
        currentScore = findViewById(R.id.current_score);

        nextButton.setOnClickListener(this);
        prevButton.setOnClickListener(this);
        trueButton.setOnClickListener(this);
        falseButton.setOnClickListener(this);

        questionList = new QuestionBank().getQuestions(new AnswerListAsyncResposne() {
            @Override
            public void processFinished(List<Question> questionArrayList) {
                System.out.println("Size is: " + questionArrayList.size());
                questionTextview.setText(questionList.get(currentQuestionIndex).getAnswer());
            }
        });

        SharedPreferences getSharedData = getPreferences(MODE_PRIVATE);
        highScore = getSharedData.getInt("highest_score", 0);
        highestScore.setText("Highest score: " + String.valueOf(highScore));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (currScore > highScore) {
            SharedPreferences.Editor editor = getPreferences(MODE_PRIVATE).edit();
            editor.putInt(MESSAGE_ID, currScore);
            editor.apply();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.prev_button:
                currentQuestionIndex = (questionList.size() + currentQuestionIndex - 1) % questionList.size();
                updateQuestion();
                break;
            case R.id.next_button:
                currentQuestionIndex = (currentQuestionIndex + 1) % questionList.size();
                updateQuestion();
                break;
            case R.id.true_button:
                checkAnswer(true);
                updateQuestion();
                break;
            case R.id.false_button:
                checkAnswer(false);
                updateQuestion();
                break;
        }

    }

    private void updateQuestion() {
        questionTextview.setText(questionList.get(currentQuestionIndex).getAnswer());
        updateQuestionsCounter();
    }

    private void updateCurrScore(boolean correctAnswer) {
        if (correctAnswer) {
            currScore = currScore + 1;

        } else {
            currScore = currScore - 1;
        }
        currentScore.setText("Current score: " + currScore);
    }

    private void checkAnswer(boolean userAnswer) {
        if (questionList.get(currentQuestionIndex).isAnswerTrue() == userAnswer) {
            updateCurrScore(true);
            Toast.makeText(MainActivity.this, R.string.correct_answer, Toast.LENGTH_SHORT).show();
            fadeView();
        } else {
            updateCurrScore(false);
            shakeAnimation();
            Toast.makeText(MainActivity.this, R.string.wrong_answer, Toast.LENGTH_SHORT).show();
        }
    }

    private void updateQuestionsCounter() {
        questionCounterTextview.setText(currentQuestionIndex + "/" + questionList.size());
    }

    private void shakeAnimation() {
        Animation shake = AnimationUtils.loadAnimation(MainActivity.this, R.anim.shake_animation);
        final CardView cardView = findViewById(R.id.cardView);
        cardView.setAnimation(shake);
        shake.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                cardView.setCardBackgroundColor(Color.RED);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                cardView.setCardBackgroundColor(Color.WHITE);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
                cardView.setCardBackgroundColor(Color.BLUE);
            }
        });
    }

    private void fadeView() {
        CardView cardView = findViewById(R.id.cardView);
        AlphaAnimation alphaAnimation = new AlphaAnimation(1.0f, 0.0f);
        alphaAnimation.setDuration(350);
        alphaAnimation.setRepeatCount(1);
        alphaAnimation.setRepeatMode(Animation.REVERSE);

        cardView.setAnimation(alphaAnimation);
        alphaAnimation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                cardView.setCardBackgroundColor(Color.GREEN);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                cardView.setCardBackgroundColor(Color.WHITE);
                currentQuestionIndex = (currentQuestionIndex + 1) % questionList.size();
                updateQuestion();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
    }
}