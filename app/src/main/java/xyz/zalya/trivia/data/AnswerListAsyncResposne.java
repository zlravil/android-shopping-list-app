package xyz.zalya.trivia.data;

import java.util.List;

import xyz.zalya.trivia.model.Question;

public interface AnswerListAsyncResposne {
    void processFinished(List<Question> questionArrayList);
}
